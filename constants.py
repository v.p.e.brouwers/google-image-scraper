categories = [
    "Bowling",
    "Baseball",
    "Basketball",
    "Softball",
    "Kickball",
    "Golfing",
    "Cricket",
    "Tennis",
    "Soccer",
    "Volleyball",
    "American football",
    "Parasailing",
    "Surfing water",
    "Water skiing",
    "Windsurfing",
    "Hurdling",
    "Parkour",
    "Archery",
    "Frisbee",
    "Disc golfing",
    "Throwing ball",
    "Throwing discus",
    "Badminton",
    "Bouncing on trampoline",
    "Cartwheeling",
    "Gymnastics tumbling",
    "Somersaulting",
    "Gymnastics Vault",
]

activities = [
    "doing archery",
    "bouncing on trampoline",
    "bowling",
    "cartwheeling",
    "catching baseball",
    "throwing baseball",
    "catching frisbee",
    "throwing frisbee",
    "catching softball",
    "throwing softball",
    "disc golfing",
    "dribbling basketball",
    "dunking basketball",
    "golf chipping",
    "golf driving",
    "golf putting",
    "doing gymnastic tumbling",
    "hitting baseball",
    "hurdling",
    "juggling soccer ball",
    "kicking field goal",
    "kicking soccer ball",
    "parasailing",
    "doing parkour",
    "passing American football",
    "passing American football",
    "playing badminton",
    "playing basketball",
    "playing cricket",
    "playing kickball",
    "playing tennis",
    "playing volleyball",
    "shooting basketball",
    "shooting goal (soccer)",
    "somersaulting",
    "surfing water",
    "throwing ball",
    "throwing discus",
    "doing gymnastics vault",
    "doing water skiing",
    "doing windsurfing",
]

subjects = [
    "kids",
    "childs",
    "toddlers",
    # "middle school",
    # "preschool",
    "primary school",
    # "elementary school",
]

# query_permutations = [
#     "{subj} playing {cat}",
#     "{subj} doing {cat}",
#     "{subj} {cat} competition",
#     "{cat} with {subj}",
# ]

query_permutations = [
    "{subj} {cat}",
    "{subj} {cat} competition",
    "{cat} with {subj}",
]

languages = [
    "en",
    "ru",  # English  # Russian
    "tr",  # Turkish
    "es",  # Spanish
    "fa",  # Persian
    "fr",  # French
    "de",  # German
    "ja",  # Japanese
    "vi",  # Vietnamese
    "zh-cn",  # Chinese (simplified)
    "zh-tw",  # Chinese (traditional)
    "ar",  # Arabian
    "pt",  # Portuguese
    "el",  # Greek
    "it",  # Italian
    "id",  # Indonesian
    "uk",  # Ukranian
    "pl",  # Polish
    "nl",  # Dutch
    "ko",  # Korean
    "iw",  # Hebrew
]
