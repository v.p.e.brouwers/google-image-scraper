import itertools

import constants

from tqdm.auto import tqdm
from tqdm.contrib.concurrent import thread_map
from google.cloud import translate_v2

import pandas as pd


def generate_query(subj, cat, template, lang, client):
    query = template.format(subj=subj, cat=cat)
    if lang == "en":
        trans_query = query
    else:
        trans_query = client.translate(query, target_language=lang, source_language="en")[
            "translatedText"
        ]

    return {
        "subject": subj,
        "category": cat,
        "template": template,
        "language": lang,
        "english_form": query,
        "translated_form": trans_query,
    }


def get_translation_client():
    return translate_v2.Client(
        target_language="nl",
    )


def generate_queries(subjects, categories, templates, languages, client=None):
    if client is None:
        client = get_translation_client()

    query_combinations = list(
        itertools.product(
            subjects,
            categories,
            templates,
        )
    )

    all_queries = []
    for lang in tqdm(languages, unit="languages", desc="Translating queries"):
        # for q_templ, subj, cat in tqdm(
        #     query_combinations, unit="queries", desc=f"Translating to {lang}"
        # ):
        #     all_queries.append(generate_query(subj, cat, q_templ, lang))
        def gen_q(items):
            subj, cat, template = items
            return generate_query(subj, cat, template, lang, client)

        all_queries += thread_map(
            gen_q,
            query_combinations,
            unit="queries",
            desc=f"Translating to {lang}",
        )

    return all_queries


if __name__ == "__main__":
    queries = generate_queries(
        subjects=constants.subjects,
        # categories=constants.categories,
        categories=constants.activities,
        templates=constants.query_permutations,
        languages=constants.languages,
    )

    df = pd.DataFrame(queries)

    df.to_pickle("queries.pkl")
    df.to_csv("queries.csv", index=False)
