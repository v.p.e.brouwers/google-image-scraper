#!/usr/bin/env python3

import argparse
import csv
import io
import os
import shutil
from multiprocessing.pool import ThreadPool
from os import path

import pandas as pd
import requests
import structlog
from numpy import imag
from PIL import Image
from tqdm.auto import tqdm

import constants
from GoogleImageScraper import GoogleImageScraper
from search_options import ColorType, ContentType, License, SafeSearch, SearchOptions, Size

log = structlog.get_logger()


def gather_for_query(query, category, language, amount=100, save_dir=None, **scraper_kwargs):
    search_opts = SearchOptions(
        query=query,
        larger_than=Size.QSVGA,
        content_type=ContentType.PHOTO,
        color_type=ColorType.FULL_COLOR,
        safe_search=SafeSearch.OFF,
        license=None,
    )

    scraper = GoogleImageScraper(
        search_options=search_opts,
        image_path=None,
        number_of_images=amount,
        min_resolution=(480, 480),
        max_resolution=None,
        **scraper_kwargs,
    )
    image_urls = scraper.find_image_urls()

    return image_urls, dict(query=query, category=category, language=language, save_dir=save_dir)


def gen_queries(csv_path=None, parquet_path=None):
    from generate_queries import generate_queries

    queries = generate_queries(
        subjects=constants.subjects,
        categories=constants.categories,
        templates=constants.query_permutations,
        languages=constants.languages,
    )

    df = pd.DataFrame(queries)

    if csv_path:
        df.to_csv(csv_path, index=False)
    if parquet_path:
        df.to_parquet(parquet_path)

    return df


def ensure_existing_filters_args(args):
    if args.language:
        assert all(
            [lang in constants.languages for lang in args.language]
        ), f"Unknown language in {args.language}"

    if args.category:
        assert all(
            [cat in constants.categories for cat in args.category]
        ), f"Unknown category in {args.category}"

    if args.subject:
        assert all(
            [subj in constants.subjects for subj in args.subject]
        ), f"Unknown category in {args.subject}"


def filter_queries(queries, discovered_images, args):
    """
    Perform some filters and transformations to decide what queries should be ran
    """

    # ascending=False, just so I have more salient data during testing
    # "American football" doesn't provide many interesting images in all but English queries
    queries = queries.sort_values(["category", "template", "language"], ascending=False)

    # We apply some filters to our query list because we do not use
    # all generated queries anymore.
    queries = queries[queries.subject.isin(constants.subjects)]

    if args.subject:
        queries = queries[queries.subject.isin(args.subject)]
    if args.category:
        queries = queries[queries.category.isin(args.category)]
    if args.language:
        queries = queries[queries.language.isin(args.language)]
    queries = queries[queries.template.isin(constants.query_permutations)]

    # Some queries translate into exactly the same forms in some languages
    queries = queries.drop_duplicates("translated_form")

    # Queries that still need to be executed
    per_query_downloads = discovered_images.search_query.value_counts()
    finished = per_query_downloads[per_query_downloads > 0].keys()
    queries = queries[~queries.translated_form.isin(finished)]

    return queries


def make_argparser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--query", action="store_true", help="Crawl google images")
    parser.add_argument("--download", action="store_true", help="Download found images")
    parser.add_argument(
        "--metadatadir",
        type=str,
        default="./queries",
        help="Folder for url and query storage",
    )
    parser.add_argument(
        "--imagedir",
        type=str,
        default="./images",
        help="Folder for image storage",
    )
    parser.add_argument(
        "--language",
        action="append",
        default=[],
        help="Control for what languages to query for, can be used multiple times. Uses all languages if not specified",
    )
    parser.add_argument(
        "--category",
        action="append",
        default=[],
        help="Control for what category to query for, can be used multiple times, uses all languages if not specified",
    )
    parser.add_argument(
        "--subject",
        action="append",
        default=[],
        help="Control for what subject types to query for, can be used multiple times, uses all languages if not specified",
    )
    parser.add_argument(
        "--workers",
        type=int,
        default=4,
        help="Control the amount of worker threads that are used.",
    )
    parser.add_argument(
        "--results-per-query",
        type=int,
        default=100,
        help="Control the amount of images collected per query.",
    )
    parser.add_argument(
        "--headless",
        action="store_true",
        help="Make the browserrun in headless mode. (always True for PhantomJS)",
    )
    parser.add_argument(
        "--webdriver-type",
        default="phantomjs",
        choices=["phantomjs", "firefox", "chrome"],
        help="Choose the Selenium backend",
    )
    parser.add_argument(
        "--regenerate-queries",
        action="store_true",
        help="Force regeneration of the translated queries permutations",
    )
    parser.add_argument(
        "--save-interval",
        default=200,
        type=int,
        help="How often the list of downloaded images gets saved to disk",
    )
    return parser


def collect_urls(
    queries,
    savedir,
    n_workers=4,
    image_store_csv=None,
    results_per_query=100,
    webdriver_type="chrome",
    headless=False,
):
    """
    Do the actual google images scraping
    """
    with ThreadPool(processes=n_workers) as executor:
        f = lambda q: gather_for_query(
            q.translated_form,
            q.category,
            q.language,
            save_dir=savedir,
            amount=results_per_query,
            webdriver_type=webdriver_type,
            headless=headless,
        )
        for image_urls, meta in tqdm(
            executor.imap_unordered(f, queries.itertuples()), total=len(queries)
        ):
            image_urls = pd.DataFrame({"url": image_urls})

            save_dir = path.join(meta["save_dir"], meta["category"], meta["language"])
            os.makedirs(save_dir, exist_ok=True)

            image_urls.to_csv(path.join(save_dir, f"{meta['query']}.csv"), index=False)
            image_urls.to_parquet(path.join(save_dir, f"{meta['query']}.parquet"))

            image_urls["search_query"] = meta["query"]

            write_headers = not os.path.exists(image_store_csv)
            image_urls.to_csv(image_store_csv, mode="a", header=write_headers, index=False)


def download_image(image_id, url, download_dir):
    error = None

    image_name = f"{image_id:09d}"
    parts = [image_name[3 * i : 3 * (i + 1)] for i in range(3)]

    dest = path.join(download_dir, *parts[:-1])
    jpeg_dest = path.join(f"{download_dir}_jpeg", *parts[:-1])
    os.makedirs(dest, exist_ok=True)

    w, h = -1, -1
    try:
        image = requests.get(url, stream=True)
        if image.status_code == 200:
            image.raw.decode_content = True

            # Wraps the raw stream so it can be used multiple times
            buf = io.BytesIO(image.raw.read())

            img = Image.open(buf)
            ext = img.format.lower()
            if ext == "jpeg":
                ext = "jpg"

            w, h = img.size

            filename = f"{parts[-1]}.{ext}"
            filename_jpeg = f"{parts[-1]}.jpg"

            filepath = path.join(dest, filename)
            filepath_jpeg = path.join(jpeg_dest, filename_jpeg)

            assert not path.exists(filepath)
            assert not path.exists(filepath_jpeg)

            img = img.convert("RGB")
            img.save(filepath_jpeg, quality=95, optimize=True)
            img.close()

            with open(filepath, "wb") as f:
                buf.seek(0)
                shutil.copyfileobj(buf, f)

        else:
            error = f"Got {image.status_code} status code"
    except AssertionError as e:
        raise e
    except Exception as e:
        error = repr(e)
        # raise e
    # requests.
    return image_id, url, w, h, error is None, error


def download(images, download_dir, image_cache, n_workers=32):
    log.info(f"Starting to download")

    with open(image_cache, "a") as f:
        writer = csv.writer(f)

        with ThreadPool(processes=n_workers) as executor:
            f = lambda x: download_image(x.image_id, x.url, download_dir)
            pbar = tqdm(
                executor.imap_unordered(f, images.itertuples()),
                total=len(images),
            )
            for image_id, w, h, url, downloaded, error in pbar:
                writer.writerow([image_id, url, downloaded, error, w, h])


def main():
    parser = make_argparser()
    args = parser.parse_args()

    ensure_existing_filters_args(args)

    url_dir = path.join(args.metadatadir, "image_urls")
    queries_csv = path.join(args.metadatadir, "queries.csv")
    discovered_imgs_csv = path.join(url_dir, "discovered_images.csv")

    image_cache_csv = path.join(args.imagedir, "images.csv")

    # Generate an all-new list of queries when required
    if not path.exists(queries_csv) or args.regenerate_queries:
        os.makedirs(args.metadatadir, exist_ok=True)
        gen_queries(queries_csv)

    # Create an empty discovered_images file if it does not exist
    if not path.exists(discovered_imgs_csv):
        os.makedirs(url_dir, exist_ok=True)
        discovered_images = pd.DataFrame(columns=["url", "search_query"])
        discovered_images.to_csv(discovered_imgs_csv, index=False)

    if not path.exists(image_cache_csv):
        os.makedirs(args.imagedir, exist_ok=True)
        with open(image_cache_csv, "w") as f:
            csv.writer(f).writerow(["image_id", "url", "downloaded", "error", "width", "height"])

    if args.query:
        discovered_images = pd.read_csv(discovered_imgs_csv)

        queries = pd.read_csv(queries_csv)
        queries = filter_queries(queries, discovered_images, args)

        log.info("Collecting images for %s queries" % len(queries))

        collect_urls(
            queries=queries,
            savedir=url_dir,
            n_workers=args.workers,
            image_store_csv=discovered_imgs_csv,
            results_per_query=args.results_per_query,
            headless=args.headless,
            webdriver_type=args.webdriver_type,
        )

    #  Disabled because it likely contains a bug and shouldn't be ran
    if args.download:
        download_list = collect_download_tasks(args, discovered_images, image_cache_csv)

        download(
            images=download_list,
            download_dir=args.imagedir,
            n_workers=args.workers,
            save_interval=args.save_interval,
        )


def collect_download_tasks(args, discovered_images, image_cache_csv):
    # These two lines are duplicated from main()
    url_dir = path.join(args.metadatadir, "image_urls")

    imgs_csv = path.join(args.imagedir, "images.csv")

    images = pd.read_csv(image_cache_csv)
    max_im_id = images.image_id.max()

    # Get all urls that aren't yet in the downloaded_images dataframe
    urls = discovered_images.url.drop_duplicates()
    urls = urls[~urls.isin(images.url)]

    image_ids, _ = pd.factorize(urls, sort=True)
    image_ids += max_im_id

    yet_to_download = pd.DataFrame({"image_id": image_ids, "url": urls})

    return yet_to_download


if __name__ == "__main__":
    main()
