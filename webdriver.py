import urllib
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def create_webdriver(driver_type="chrome", headless=False, driver_path=None):
    driver_kwargs = {}

    if driver_type == "chrome":
        driver_kwargs["options"] = webdriver.ChromeOptions()
        driver = webdriver.Chrome
    elif driver_type == "firefox":
        driver_kwargs["options"] = webdriver.FirefoxOptions()
        driver = webdriver.Firefox
    elif driver_type == "phantomjs":
        driver = webdriver.PhantomJS
    else:
        raise ValueError("args.webdriver contains an unknown driver type")

    if driver_path:
        driver_kwargs["executable_path"] = driver_path
    if headless and "options" in driver_kwargs:
        driver_kwargs["options"].add_argument("--headless")
        driver_kwargs["options"].add_argument("--disable-gpu")

    driver = driver(**driver_kwargs)

    return driver
