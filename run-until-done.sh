#!/bin/bash

JOB=$1

shift

until $JOB $@ ; do
    echo "Job failed, killing remnants" >&2
    pkill -e -9 geckodriver
    pkill -e -9 firefox-bin
    echo "Remnants killed, restarting in 1 second."
    sleep 1
done
