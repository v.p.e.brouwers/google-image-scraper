import warnings
from dataclasses import dataclass
from enum import Enum
from urllib.parse import urlencode


class SearchOption(Enum):
    def __str__(self):
        """
        Overriding the str method allows these enums to be easily used interchangably
        with string arguments.
        """
        return str(self.value)


class Size(SearchOption):
    QSVGA = "qsvga"  # 400x300
    VGA = "vga"  # 640x480
    SVGA = "svga"  # 800x600
    XGA = "xga"  # 1024x768
    # There are many others which can be implemented


class SizeType(SearchOption):
    LARGE = "l"
    MEDIUM = "m"
    ICON = "i"
    LARGER_THAN = "lt"


class ContentType(SearchOption):
    PHOTO = "photo"
    FACE = "face"
    CLIPART = "clipart"
    LINEART = "lineart"
    ANIMATION = "animated"


class ColorType(SearchOption):
    FULL_COLOR = "color"
    GRAYSCALEd = "gray"
    TRANSPARENT = "trans"
    SPECIFIC = "specific"


class SpecificColor(SearchOption):
    RED = "red"
    ORANGE = "orange"
    YELLOW = "yellow"
    GREEN = "green"
    TEAL = "teal"
    BLUE = "blue"
    PURPLE = "purple"
    PINK = "pink"
    WHITE = "white"
    GRAY = "gray"
    BLACK = "black"
    BROWN = "brown"


class SafeSearch(SearchOption):
    OFF = "images"
    ON = "active"


class License(SearchOption):
    CREATIVE_COMMONS = "cl"
    COMMERCIAL = "ol"


class ImageFormat(SearchOption):
    JPEG = "jpeg"
    GIF = "gif"
    PNG = "png"
    BMP = "bmp"
    SVG = "svg"
    WEBP = "webp"
    ICO = "ico"
    RAW = "raw"


@dataclass
class SearchOptions:
    query: str
    country: str = None
    size_type: SizeType = None
    larger_than: Size = None
    content_type: ContentType = None
    color_type: ColorType = None
    color_theme: SpecificColor = None
    image_format: ImageFormat = None
    safe_search: SafeSearch = None
    license: License = None

    def arg_dict(self):
        assert self.query is not None

        args = {}
        tbs_args = {}

        # Makes sure google searches for images
        args["tbm"] = "isch"

        args["q"] = self.query

        if self.country:
            args["cr"] = self.country

        if self.safe_search:
            args["safe"] = str(self.safe_search)

        if self.larger_than:
            if self.size_type and self.size_type != SizeType.LARGER_THAN:
                warnings.warn(
                    "larger_than is only used when size_type is SizeType.LARGER_THAN or None"
                )
            tbs_args["islt"] = str(self.larger_than)

        if self.size_type:
            tbs_args["isz"] = str(self.size_type)

        if self.content_type:
            tbs_args["itp"] = str(self.content_type)

        if self.color_theme:
            if self.color_type and self.color_type != ColorType.SPECIFIC:
                warnings.warn(
                    "color_theme is only used when color_type is ColorType.SPECIFIC or None"
                )
            tbs_args["isc"] = str(self.color_theme)

        if self.color_type:
            tbs_args["ic"] = str(self.color_type)

        if self.image_format:
            tbs_args["ift"] = str(self.image_format)

        if self.license:
            tbs_args["sur"] = str(self.license)

        if tbs_args:
            args["tbs"] = ",".join(f"{k}:{v}" for k, v in tbs_args.items())

        return args

    def as_querystring(self):
        args = self.arg_dict()

        return urlencode(args)
