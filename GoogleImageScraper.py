# -*- coding: utf-8 -*-
"""
Created on Sat Jul 18 13:01:02 2020

@author: OHyic
"""
# import selenium drivers
from search_options import SearchOptions
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

# import helper libraries
import time
import urllib.request
import os
import requests
import structlog
from PIL import Image


try:
    import chromedriver_binary
except:
    pass

from webdriver import create_webdriver

log = structlog.get_logger()


class GoogleImageScraper:
    def __init__(
        self,
        search_options,
        image_path=None,
        # search_key="cat",
        number_of_images=1,
        webdriver_type="chrome",
        headless=False,
        webdriver_path=None,
        min_resolution=(480, 480),
        max_resolution=(1920, 1080),
    ):
        # check parameter types
        if type(number_of_images) != int:
            print("[Error] Number of images must be integer value.")
            return
        if image_path is not None and not os.path.exists(image_path):
            print("[INFO] Image path not found. Creating a new folder.")
            os.makedirs(image_path)
        # check if chromedriver is updated

        self.driver = create_webdriver(webdriver_type, headless, webdriver_path)
        self.driver.implicitly_wait(20)

        self.search_key = search_options.query
        self.number_of_images = number_of_images
        self.image_path = image_path

        querystring = search_options.as_querystring()
        # self.url = "https://www.google.com/search?tbm=isch&q=%s" % (search_key)
        self.url = "https://www.google.com/search?%s" % (querystring)

        self.headless = headless
        self.min_resolution = min_resolution
        self.max_resolution = max_resolution

        self.saved_extension = "jpg"
        self.valid_extensions = ["jpg", "png", "jpeg"]

        # Every 25th "image" item
        self.non_image_interval = 25

        self.log = log.bind(query=self.search_key)

    def find_image_urls(self):
        """
        This function search and return a list of image urls based on the search key.
        Example:
            google_image_scraper = GoogleImageScraper("webdriver_path","image_path","search_key",number_of_photos)
            image_urls = google_image_scraper.find_image_urls()

        """
        self.log.info("Scraping for image link.")
        image_urls = []
        count = 0
        missed_count = 0

        driver = self.driver

        driver.get(self.url)

        wait = WebDriverWait(driver, 20)

        non_image_pad = self.number_of_images // self.non_image_interval
        adjusted_n_children = self.number_of_images + non_image_pad

        for indx in range(1, adjusted_n_children + 1):
            try:
                img_thumb = self.driver.find_element_by_css_selector(
                    f"#islrg > div.islrc > div:nth-child({indx})"
                )
                driver.execute_script("arguments[0].scrollIntoView();", img_thumb)

                # Check if this is one of the non-image "related search" blocks.
                # These should happen every 25th (self.non_image_interval) image
                if "J3Tg1d" in img_thumb.get_attribute("class").split():
                    continue
                else:
                    # If not, we can safely assume it contains an image
                    img_thumb = img_thumb.find_element_by_css_selector("img")

                img_thumb.click()
                missed_count = 0
            except Exception as e:
                # print("[-] Unable to click this photo.")
                self.log.debug(f"Missed photo: {e}")
                missed_count = missed_count + 1
                if missed_count > 10:
                    self.log.info("No more photos.")
                    break
                else:
                    continue

            try:
                # Wait until image preview is visible
                img_preview = self.driver.find_element_by_css_selector(".BIB1wf")
                wait.until(EC.visibility_of(img_preview))
                # Wait until image loading bar has disappeared
                loading_bar = img_preview.find_element_by_css_selector(".BIB1wf .k7O2sd")
                wait.until(EC.invisibility_of_element(loading_bar))
                # Then select the actual image

                image = img_preview.find_element_by_css_selector(".BIB1wf .n3VNCb")
                url = image.get_attribute("src")

                if url.startswith("http"):
                    image_urls.append(url)
                    count += 1
            except Exception as e:
                self.log.warning(f"Unable to get link: {repr(e)}")
            element = self.driver.find_element_by_class_name("mye4qd")

            if element.is_displayed():
                element.click()
                print("[INFO] Loading more photos")

        self.driver.quit()
        self.log.info("Google search ended")
        return image_urls

    def save_images(self, image_urls):
        # save images into file directory
        """
        This function takes in an array of image urls and save it into the prescribed image path/directory.
        Example:
            google_image_scraper = GoogleImageScraper("webdriver_path","image_path","search_key",number_of_photos)
            image_urls=["https://example_1.jpg","https://example_2.jpg"]
            google_image_scraper.save_images(image_urls)

        """

        if self.image_path is None:
            self.log.error("Attempted to save images, but no image_path is provided")
            raise ValueError()

        self.log.info("Saving Images")
        for indx, image_url in enumerate(image_urls):
            try:
                filename = "%s%s.%s" % (self.search_key, str(indx), self.saved_extension)
                image_path = os.path.join(self.image_path, filename)
                self.log.debug("%d .Image saved at: %s" % (indx, image_path))
                image = requests.get(image_url)
                if image.status_code == 200:
                    image.raw.decode_content = True  # handle spurious Content-Encoding
                    image_from_web = Image.open(image.raw)

                    image_from_web = Image.open(image_path)
                    image_resolution = image_from_web.size
                    # What does this check do? Wouldn't size=None automatically mean the image is corrupt?
                    # if image_resolution != None:
                    if (
                        self.min_resolution
                        and (
                            image_resolution[0] < self.min_resolution[0]
                            or image_resolution[1] < self.min_resolution[1]
                        )
                    ) or (
                        self.max_resolution
                        and (
                            image_resolution[0] > self.max_resolution[0]
                            or image_resolution[1] > self.max_resolution[1]
                        )
                    ):
                        self.logger.debug("%s did not meet resolution requirements." % (image_url))
                    else:
                        with open(image_path, "wb") as f:
                            f.write(image.content)
                            f.close()

                    image_from_web.close()

            except Exception as e:
                self.log.warning("[ERROR] Failed to be downloaded", e)
                pass
        self.log.info(
            "Download Completed. Please note that some photos are not downloaded as it is not in the right format (e.g. jpg, jpeg, png)"
        )
